package com.tpom6oh.ratesapp.logic.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.content.SharedPreferences
import com.tpom6oh.ratesapp.logic.util.CurrentTime
import com.tpom6oh.ratesapp.model.entity.Currency
import com.tpom6oh.ratesapp.model.entity.CurrencyViewData
import com.tpom6oh.ratesapp.model.repository.RatesRepository
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.TestScheduler
import spock.lang.Specification

import java.util.concurrent.TimeUnit
/**
 * Created by MI on 02.09.2017.
 */
class RatesViewModelTest extends Specification {

    RatesViewModel sut
    TestScheduler testScheduler = new TestScheduler()

    void setup() {
        sut = new RatesViewModel()

        sut.ratesRepository = Mock(RatesRepository)
        sut.computationScheduler = testScheduler
        sut.sharedPreferences = Mock(SharedPreferences)
        sut.currentTime = Mock(CurrentTime)
        sut.incomeAmountData = Mock(MutableLiveData)
        sut.outcomeAmountData = Mock(MutableLiveData)
    }

    def "Should ask repository to sync rates every 30 seconds"() {
        given:
        sut.currentTime.currentTimeMillis() >> System.currentTimeMillis()
        sut.sharedPreferences.getLong(_, _) >> 0

        when:
        sut.syncRepository()
        testScheduler.triggerActions()
        then:
        1 * sut.ratesRepository.syncRates()

        when:
        testScheduler.advanceTimeBy(30, TimeUnit.SECONDS)

        then:
        1 * sut.ratesRepository.syncRates()
    }

    def "Should wait until 30 seconds since last sync pass before requesting repository to sync rates on start"() {
        given:
        sut.currentTime.currentTimeMillis() >> 11000
        sut.sharedPreferences.getLong(_, _) >> 10000

        when:
        sut.syncRepository()
        testScheduler.triggerActions()
        then:
        0 * sut.ratesRepository.syncRates()

        when:
        testScheduler.advanceTimeBy(28, TimeUnit.SECONDS)

        then:
        0 * sut.ratesRepository.syncRates()

        when:
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)

        then:
        1 * sut.ratesRepository.syncRates()
    }

    def "Should track income focus"() {
        when:
        sut.onIncomeAmountFocused()

        then:
        assert TransactionDirection.Income == sut.transactionDirection
    }

    def "Should track outcome focus"() {
        when:
        sut.onOutcomeAmountFocused()

        then:
        assert TransactionDirection.Outcome == sut.transactionDirection
    }

    def "Should set outcome transaction direction on outcome amount input"() {
        given:
        sut.ratesRepository.getRate(_,_,_) >> Flowable.empty()

        when:
        sut.onOutcomeAmountInput(100.0)

        then:
        assert TransactionDirection.Outcome == sut.transactionDirection
    }

    def "Should post income and outcome amounts on outcome amount imput"() {
        given:
        sut.outcomeCurrency = Currency.GBP
        sut.incomeCurrency = Currency.RUB
        double gbpToRubRate = 79.0
        double outcomeAmount = 100.0
        sut.ratesRepository.getRate(Currency.GBP, Currency.RUB, false) >> Flowable.just(gbpToRubRate)

        when:
        sut.onOutcomeAmountInput(outcomeAmount)

        then:
        assert sut.rateDisposable != null
        1 * sut.incomeAmountData.postValue(outcomeAmount * gbpToRubRate)
        1 * sut.outcomeAmountData.postValue(outcomeAmount)
    }

    def "Should set income transaction direction on income amount input"() {
        given:
        sut.ratesRepository.getRate(_,_,_) >> Flowable.empty()

        when:
        sut.onIncomeAmountInput(100.0)

        then:
        assert TransactionDirection.Income == sut.transactionDirection
    }

    def "Should post income and outcome amounts on income amount input"() {
        given:
        sut.outcomeCurrency = Currency.GBP
        sut.incomeCurrency = Currency.RUB
        double rubToGbpRate = 0.015
        double incomeAmount = 100.0
        sut.ratesRepository.getRate(Currency.RUB, Currency.GBP, false) >> Flowable.just(rubToGbpRate)

        when:
        sut.onIncomeAmountInput(incomeAmount)

        then:
        assert sut.rateDisposable != null
        1 * sut.incomeAmountData.postValue(incomeAmount)
        1 * sut.outcomeAmountData.postValue(incomeAmount * rubToGbpRate)
    }

    def "Should save outcome currency on outcome currency change"() {
        given:
        sut.transactionDirection = TransactionDirection.Outcome
        when:
        sut.onOutcomeCurrencyChange(new CurrencyViewData(Currency.CHF))
        then:
        assert Currency.CHF == sut.outcomeCurrency
    }

    def "Should reset amount on outcome currency change if outcome transaction"() {
        given:
        sut.transactionDirection = TransactionDirection.Outcome
        when:
        sut.onOutcomeCurrencyChange(new CurrencyViewData(Currency.CHF))
        then:
        1 * sut.incomeAmountData.postValue(0.0)
        1 * sut.outcomeAmountData.postValue(0.0)
    }

    def "Should post new outcome amount on outcome currency change if income transaction"() {
        given:
        double exchangeRate = 60.0
        double incomeAmount = 2.5
        sut.incomeAmountData.value >> incomeAmount
        sut.incomeCurrency = Currency.USD
        sut.outcomeCurrency = Currency.RUB
        sut.transactionDirection = TransactionDirection.Income
        sut.ratesRepository.getRate(Currency.USD, Currency.RUB, true) >> Flowable.just(exchangeRate)
        when:
        sut.onOutcomeCurrencyChange(new CurrencyViewData(Currency.RUB))
        then:
        assert sut.rateDisposable != null
        1 * sut.outcomeAmountData.setValue(incomeAmount * exchangeRate)
    }

    def "Should save income currency on income currency change"() {
        given:
        sut.transactionDirection = TransactionDirection.Income
        when:
        sut.onIncomeCurrencyChange(new CurrencyViewData(Currency.CHF))
        then:
        assert Currency.CHF == sut.incomeCurrency
    }

    def "Should reset amount on income currency change if income transaction"() {
        given:
        sut.transactionDirection = TransactionDirection.Income
        when:
        sut.onIncomeCurrencyChange(new CurrencyViewData(Currency.CHF))
        then:
        1 * sut.incomeAmountData.postValue(0.0)
        1 * sut.outcomeAmountData.postValue(0.0)
    }

    def "Should post new income amount on income currency change if outcome transaction"() {
        given:
        double exchangeRate = 0.2
        double outcomeAmount = 250
        sut.outcomeAmountData.value >> outcomeAmount
        sut.incomeCurrency = Currency.USD
        sut.outcomeCurrency = Currency.RUB
        sut.transactionDirection = TransactionDirection.Outcome
        sut.ratesRepository.getRate(Currency.RUB, Currency.USD, true) >> Flowable.just(exchangeRate)
        when:
        sut.onIncomeCurrencyChange(new CurrencyViewData(Currency.USD))
        then:
        assert sut.rateDisposable != null
        1 * sut.incomeAmountData.setValue(outcomeAmount * exchangeRate)
    }

    def "Should dispose disposables on cleared"() {
        given:
        sut.rateDisposable = Mock(Disposable)
        sut.syncRatesDisposable = Mock(Disposable)

        when:
        sut.onCleared()

        then:
        1 * sut.rateDisposable.dispose()
        1 * sut.syncRatesDisposable.dispose()
        1 * sut.ratesRepository.dispose()
    }
}
