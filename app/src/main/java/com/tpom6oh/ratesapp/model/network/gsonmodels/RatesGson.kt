package com.tpom6oh.ratesapp.model.network.gsonmodels

/**
 * Created by MI on 27.08.2017.
 */
class RatesGson(var base: String?,
                var date: String?,
                var rates: Map<String, Double>?)