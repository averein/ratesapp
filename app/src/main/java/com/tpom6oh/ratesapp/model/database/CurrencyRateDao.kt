package com.tpom6oh.ratesapp.model.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Flowable


/**
 * Created by MI on 27.08.2017.
 */
@Dao
interface CurrencyRateDao {
    @Insert(onConflict = REPLACE)
    fun save(currencyRates: List<CurrencyRate>)

    @Query("SELECT * FROM currency_rates WHERE outcomeCurrency = :outcomeCurrency AND incomeCurrency = :incomeCurrency")
    fun load(outcomeCurrency: String, incomeCurrency: String): Flowable<CurrencyRate>
}