package com.tpom6oh.ratesapp.model.entity

enum class Currency {
    GBP,
    EUR,
    USD,
    PLN,
    CHF,
    RUB
}