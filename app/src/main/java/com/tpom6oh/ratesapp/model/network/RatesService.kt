package com.tpom6oh.ratesapp.model.network

import com.tpom6oh.ratesapp.model.network.gsonmodels.RatesGson
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by MI on 27.08.2017.
 */
interface RatesService {

    @GET("latest")
    fun getRate(@Query("base") currencyCode: String): Single<RatesGson>

}