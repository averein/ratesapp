package com.tpom6oh.ratesapp.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase



/**
 * Created by MI on 27.08.2017.
 */
@Database(entities = arrayOf(CurrencyRate::class), version = 1)
abstract class CurrencyRateDatabase : RoomDatabase() {
    abstract fun currencyRateDao(): CurrencyRateDao
}