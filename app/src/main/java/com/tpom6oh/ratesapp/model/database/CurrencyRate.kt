package com.tpom6oh.ratesapp.model.database

import android.arch.persistence.room.Entity


/**
 * Created by MI on 27.08.2017.
 */

@Entity(tableName="currency_rates",
        primaryKeys = arrayOf("outcomeCurrency", "incomeCurrency"))
class CurrencyRate(
    var outcomeCurrency: String?,
    var incomeCurrency: String?,
    var exchangeRate: Double
)