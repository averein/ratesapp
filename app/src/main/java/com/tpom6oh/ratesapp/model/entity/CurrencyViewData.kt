package com.tpom6oh.ratesapp.model.entity

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by MI on 27.08.2017.
 */

data class CurrencyViewData(val currency: Currency) : Parcelable {
    constructor(source: Parcel) : this(
            Currency.values()[source.readInt()]
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(currency.ordinal)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CurrencyViewData> = object : Parcelable.Creator<CurrencyViewData> {
            override fun createFromParcel(source: Parcel): CurrencyViewData = CurrencyViewData(source)
            override fun newArray(size: Int): Array<CurrencyViewData?> = arrayOfNulls(size)
        }
    }
}