package com.tpom6oh.ratesapp.model.repository

import android.content.SharedPreferences
import android.util.Log
import com.tpom6oh.ratesapp.di.qualifier.ComputationScheduler
import com.tpom6oh.ratesapp.di.qualifier.IoScheduler
import com.tpom6oh.ratesapp.di.qualifier.MainScheduler
import com.tpom6oh.ratesapp.model.database.CurrencyRate
import com.tpom6oh.ratesapp.model.database.CurrencyRateDao
import com.tpom6oh.ratesapp.model.entity.Currency
import com.tpom6oh.ratesapp.model.network.RatesService
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by MI on 27.08.2017.
 */
open class RatesRepository @Inject constructor(private val sharedPreferences: SharedPreferences,
                                          private val currencyRateDao: CurrencyRateDao,
                                          private val ratesService: RatesService,
                                          @IoScheduler
                                          private val ioScheduler: Scheduler,
                                          @ComputationScheduler
                                          private val computationScheduler: Scheduler,
                                          @MainScheduler
                                          private val mainScheduler: Scheduler) {

    companion object {
        val ratesUpdateTimestampKey = "RATES_UPDATE_TIMESTAMP_KEY"
    }

    private var rateFetchDisposable: CompositeDisposable = CompositeDisposable()

    val currencies = Currency.values().toList()

    open fun getRate(fromCurrency: Currency, toCurrency: Currency, mainThread: Boolean = false):
            Flowable<Double> = currencyRateDao.load(fromCurrency.name, toCurrency.name)
                .subscribeOn(ioScheduler)
                .observeOn(if (mainThread) mainScheduler else computationScheduler)
                .map { it.exchangeRate }

    open fun syncRates() {
        currencies.forEach { fromCurrency ->
            rateFetchDisposable.add(ratesService.getRate(fromCurrency.name)
                    .subscribeOn(ioScheduler)
                    .subscribe({ conversionRates ->
                        val currRates = conversionRates.rates
                        if (currRates != null) {
                            val currencyRatesToDb = currRates.map { mapEntry ->
                                CurrencyRate(fromCurrency.name, mapEntry.key, mapEntry.value)
                            }.toMutableList()
                            currencyRatesToDb.add(CurrencyRate(fromCurrency.name, fromCurrency.name, 1.0))
                            currencyRateDao.save(currencyRatesToDb)
                            sharedPreferences.edit().putLong(ratesUpdateTimestampKey,
                                    System.currentTimeMillis()).apply()
                        }
                    }, { e ->
                        Log.e(RatesRepository::class.java.simpleName, "Failed to sync rates", e)
                    }))
        }
    }

    open fun dispose() {
        rateFetchDisposable.dispose()
    }
}

