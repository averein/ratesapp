package com.tpom6oh.ratesapp.di.component

import com.tpom6oh.ratesapp.di.module.ApplicationModule
import com.tpom6oh.ratesapp.di.module.NetworkModule
import com.tpom6oh.ratesapp.logic.viewmodel.RatesViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by MI on 02.09.2017.
 */
@Singleton
@Component(modules = arrayOf(
        ApplicationModule::class,
        NetworkModule::class))
interface AppComponent {
    fun inject(ratesViewModel: RatesViewModel)
}