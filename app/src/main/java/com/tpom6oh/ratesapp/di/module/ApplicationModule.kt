package com.tpom6oh.ratesapp.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import com.tpom6oh.ratesapp.di.qualifier.ComputationScheduler
import com.tpom6oh.ratesapp.di.qualifier.IoScheduler
import com.tpom6oh.ratesapp.di.qualifier.MainScheduler
import com.tpom6oh.ratesapp.model.database.CurrencyRateDao
import com.tpom6oh.ratesapp.model.database.CurrencyRateDatabase
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

/**
 * Created by MI on 02.09.2017.
 */

@Module
class ApplicationModule(val application: Application) {

    @Provides
    fun provideApplication(): Application = application

    @Provides
    fun provideSharedPreferences(application: Application): SharedPreferences
            = application.getSharedPreferences("mainprefs", Context.MODE_PRIVATE)

    @Singleton
    @Provides
    fun provideDatabase(application: Application): CurrencyRateDatabase {
        val databaseName = "currency-rate-db"
        return Room.databaseBuilder(application,
                CurrencyRateDatabase::class.java, databaseName).build()
    }

    @Provides
    fun provideCurrencyRateDao(currencyRateDatabase: CurrencyRateDatabase): CurrencyRateDao =
            currencyRateDatabase.currencyRateDao()

    @MainScheduler
    @Provides
    fun provideMainScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @IoScheduler
    @Provides
    fun provideIoScheduler(): Scheduler = Schedulers.io()

    @ComputationScheduler
    @Provides
    fun provideComputationSchduler(): Scheduler = Schedulers.computation()
}