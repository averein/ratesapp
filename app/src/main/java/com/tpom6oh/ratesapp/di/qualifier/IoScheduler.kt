package com.tpom6oh.ratesapp.di.qualifier

import javax.inject.Qualifier

/**
 * Created by MI on 02.09.2017.
 */
@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class IoScheduler