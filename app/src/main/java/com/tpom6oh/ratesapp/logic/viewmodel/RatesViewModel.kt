package com.tpom6oh.ratesapp.logic.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.SharedPreferences
import android.util.Log
import com.tpom6oh.ratesapp.di.qualifier.ComputationScheduler
import com.tpom6oh.ratesapp.logic.util.CurrentTime
import com.tpom6oh.ratesapp.model.entity.Currency
import com.tpom6oh.ratesapp.model.entity.CurrencyViewData
import com.tpom6oh.ratesapp.model.repository.RatesRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by MI on 27.08.2017.
 */
class RatesViewModel: ViewModel() {

    var rateDisposable: Disposable? = null
    var syncRatesDisposable: Disposable? = null

    var transactionDirection = TransactionDirection.None

    var outcomeCurrency = Currency.GBP
    var incomeCurrency = Currency.GBP

    var incomeAmountData = MutableLiveData<Double>()
    var outcomeAmountData = MutableLiveData<Double>()

    @Inject
    lateinit var sharedPreferences: SharedPreferences
    @Inject
    lateinit var ratesRepository: RatesRepository
    @field:[Inject ComputationScheduler]
    lateinit var computationScheduler: Scheduler
    @Inject
    lateinit var currentTime: CurrentTime

    private val ratesSyncFrequency = 30L

    fun syncRepository() {
        val initialDelay = calculateInitialSyncDelay()

        syncRatesDisposable = Observable.interval(initialDelay, ratesSyncFrequency,
                TimeUnit.SECONDS, computationScheduler)
                .subscribe({
                    ratesRepository.syncRates()
                }, { e ->
                    Log.e(RatesViewModel::class.java.simpleName, "Failed to update rates", e)
                })
    }

    private fun calculateInitialSyncDelay(): Long {
        val ratesSyncTimestamp = sharedPreferences
                .getLong(RatesRepository.ratesUpdateTimestampKey, 0)
        val timeSinceRatesSync = currentTime.currentTimeMillis() - ratesSyncTimestamp

        val convert = TimeUnit.SECONDS.convert(timeSinceRatesSync,
                TimeUnit.MILLISECONDS)
        return Math.max(0, ratesSyncFrequency - convert)
    }

    fun getCurrenciesData() = ratesRepository.currencies.map { CurrencyViewData(it) }

    fun onOutcomeAmountFocused() {
        transactionDirection = TransactionDirection.Outcome
    }

    fun onIncomeAmountFocused() {
        transactionDirection = TransactionDirection.Income
    }

    fun onOutcomeAmountInput(amount: Double) {
        transactionDirection = TransactionDirection.Outcome
        withRate { rate ->
            incomeAmountData.postValue(amount * rate)
            outcomeAmountData.postValue(amount)
        }
    }

    fun onIncomeAmountInput(amount: Double) {
        transactionDirection = TransactionDirection.Income
        withRate { rate ->
            outcomeAmountData.postValue(amount * rate)
            incomeAmountData.postValue(amount)
        }
    }

    fun onOutcomeCurrencyChange(newOutcomeCurrency: CurrencyViewData) {
        outcomeCurrency = newOutcomeCurrency.currency

        if (transactionDirection == TransactionDirection.Outcome) {
            incomeAmountData.postValue(0.0)
            outcomeAmountData.postValue(0.0)
        } else {
            withRateOnMainThread { rate ->
                outcomeAmountData.value = incomeAmountData.value?.times(rate)
            }
        }
    }

    fun onIncomeCurrencyChange(newIncomeCurrency: CurrencyViewData) {
        incomeCurrency = newIncomeCurrency.currency

        if (transactionDirection == TransactionDirection.Income) {
            incomeAmountData.postValue(0.0)
            outcomeAmountData.postValue(0.0)
        } else {
            withRateOnMainThread { rate ->
                incomeAmountData.value = outcomeAmountData.value?.times(rate)
            }
        }
    }

    private fun withRate(onSuccess: (rate: Double) -> Unit) {
        withRate(onSuccess, false)
    }

    private fun withRateOnMainThread(onSuccess: (rate: Double) -> Unit) {
        withRate(onSuccess, true)
    }

    private fun withRate(onSuccess: (rate: Double) -> Unit, mainThread: Boolean) {
        rateDisposable?.dispose()

        rateDisposable = getRateForCurrentTransactionDirection(mainThread)
                .subscribe({ rate ->
                    onSuccess(rate)
                }, { e ->
                    Log.e(RatesViewModel::class.java.simpleName, "Failed to get rate", e)
                })
    }

    private fun getRateForCurrentTransactionDirection(mainThread: Boolean): Flowable<Double> {
        return if (transactionDirection == TransactionDirection.Outcome) {
            ratesRepository.getRate(outcomeCurrency, incomeCurrency, mainThread)
        } else {
            ratesRepository.getRate(incomeCurrency, outcomeCurrency, mainThread)
        }
    }

    override fun onCleared() {
        super.onCleared()

        ratesRepository.dispose()
        rateDisposable?.dispose()
        syncRatesDisposable?.dispose()
    }
}

