package com.tpom6oh.ratesapp.logic.util

import javax.inject.Inject

/**
 * Created by MI on 02.09.2017.
 */
open class CurrentTime @Inject constructor() {

    open fun currentTimeMillis(): Long = System.currentTimeMillis()

}