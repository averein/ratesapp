package com.tpom6oh.ratesapp.logic

import android.app.Application
import com.tpom6oh.ratesapp.di.component.AppComponent
import com.tpom6oh.ratesapp.di.component.DaggerAppComponent
import com.tpom6oh.ratesapp.di.module.ApplicationModule

/**
 * Created by MI on 02.09.2017.
 */
class RatesApplication: Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }
}