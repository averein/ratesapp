package com.tpom6oh.ratesapp.logic.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.tpom6oh.ratesapp.model.entity.CurrencyViewData
import com.tpom6oh.ratesapp.ui.OutcomeCurrencyCardFragment

class OutcomeCurrencyFragmentPagerAdapter(
        val data: List<CurrencyViewData>,
        fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment =
            OutcomeCurrencyCardFragment.create(data[position])

    override fun getCount(): Int = data.size
}