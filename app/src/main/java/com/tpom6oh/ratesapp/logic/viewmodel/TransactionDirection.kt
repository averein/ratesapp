package com.tpom6oh.ratesapp.logic.viewmodel

enum class TransactionDirection {
    None,
    Outcome,
    Income
}