package com.tpom6oh.ratesapp.ui

import android.arch.lifecycle.LiveData
import android.os.Bundle
import com.tpom6oh.ratesapp.model.entity.CurrencyViewData

/**
 * Created by MI on 02.09.2017.
 */
class IncomeCurrencyCardFragment : CurrencyCardFragment() {

    companion object {
        fun create(currencyViewData: CurrencyViewData): CurrencyCardFragment {
            val fragment = IncomeCurrencyCardFragment()
            val args = Bundle()
            args.putParcelable(currencyDataKey, currencyViewData)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAmountFocused() {
        viewModel.onIncomeAmountFocused()
    }

    override fun dataToObserve(): LiveData<Double> = viewModel.incomeAmountData

    override fun onAmountInput(amount: Double) {
        viewModel.onIncomeAmountInput(amount)
    }


}