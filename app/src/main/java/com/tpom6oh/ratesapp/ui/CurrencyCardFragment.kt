package com.tpom6oh.ratesapp.ui

import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tpom6oh.ratesapp.R
import com.tpom6oh.ratesapp.logic.viewmodel.RatesViewModel
import com.tpom6oh.ratesapp.model.entity.CurrencyViewData

/**
 * Created by MI on 27.08.2017.
 */
abstract class CurrencyCardFragment : LifecycleFragment() {

    companion object {
        internal val currencyDataKey = "Currency Data Key"
    }

    private lateinit var amountText: TextView
    private lateinit var currencyNameText: TextView

    private val textWatcher = CurrencyTextWatcher()

    internal lateinit var viewModel: RatesViewModel
    private lateinit var currencyViewData: CurrencyViewData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity).get(RatesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        readArguments()

        val viewGroup = inflater?.inflate(R.layout.currency_card, container, false) as ViewGroup
        findViews(viewGroup)
        showCurrencyName(currencyViewData)

        setupAmountInteractions()

        observeAmountChange()

        return viewGroup
    }

    private fun readArguments() {
        currencyViewData = arguments.getParcelable(currencyDataKey)
    }

    private fun findViews(viewGroup: ViewGroup) {
        amountText = viewGroup.findViewById(R.id.exchangeAmount)
        currencyNameText = viewGroup.findViewById(R.id.currencyName)
    }


    private fun showCurrencyName(currencyViewData: CurrencyViewData) {
        currencyNameText.text = currencyViewData.currency.name
    }

    private fun setupAmountInteractions() {
        amountText.addTextChangedListener(textWatcher)
        amountText.onFocusChangeListener = View.OnFocusChangeListener { _, _ ->
            onAmountFocused()
        }
    }

    private fun observeAmountChange() {
        dataToObserve().observe(this, Observer<Double> {
            amountText.removeTextChangedListener(textWatcher)
            val oldValue = try {
                amountText.text.toString().toDouble()
            } catch (e: NumberFormatException) {
                0
            }
            if (it != null && it != oldValue) {
                amountText.text = getString(R.string.money_amount, it)
            }
            amountText.addTextChangedListener(textWatcher)
        })
    }

    abstract fun onAmountFocused()
    abstract fun dataToObserve(): LiveData<Double>
    abstract fun onAmountInput(amount: Double)

    inner class CurrencyTextWatcher: TextWatcher {

        override fun afterTextChanged(p0: Editable?) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if (amountText.hasFocus()) {
                val amount = try {
                    p0.toString().toDouble()
                } catch (e: NumberFormatException) {
                    0.0
                }

                onAmountInput(amount)
            }
        }
    }
}
