package com.tpom6oh.ratesapp.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.tpom6oh.ratesapp.R
import com.tpom6oh.ratesapp.logic.RatesApplication
import com.tpom6oh.ratesapp.logic.adapter.IncomeCurrencyFragmentPagerAdapter
import com.tpom6oh.ratesapp.logic.adapter.OutcomeCurrencyFragmentPagerAdapter
import com.tpom6oh.ratesapp.logic.viewmodel.RatesViewModel

class RatesActivity : AppCompatActivity() {

    private lateinit var outcomeCurrencyPager: ViewPager
    private lateinit var incomeCurrencyPager: ViewPager
    private lateinit var exchangeRate: TextView

    private lateinit var ratesViewModel: RatesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_rates)
        findViews()

        initViewModel()

        setupOutcomeViewPager()
        setupIncomeViewPager()
    }

    private fun findViews() {
        outcomeCurrencyPager = findViewById(R.id.outcomeViewPager)
        incomeCurrencyPager = findViewById(R.id.incomeViewPager)
        exchangeRate = findViewById(R.id.rateTextView)
    }

    private fun initViewModel() {
        ratesViewModel = ViewModelProviders.of(this).get(RatesViewModel::class.java)
        RatesApplication.appComponent.inject(ratesViewModel)
        ratesViewModel.syncRepository()
    }

    private fun setupOutcomeViewPager() {
        outcomeCurrencyPager.adapter = OutcomeCurrencyFragmentPagerAdapter(
                ratesViewModel.getCurrenciesData(), supportFragmentManager)
        outcomeCurrencyPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                ratesViewModel.onOutcomeCurrencyChange(ratesViewModel.getCurrenciesData()[position])
            }
        })
    }

    private fun setupIncomeViewPager() {
        incomeCurrencyPager.adapter = IncomeCurrencyFragmentPagerAdapter(
                ratesViewModel.getCurrenciesData(), supportFragmentManager)
        incomeCurrencyPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                ratesViewModel.onIncomeCurrencyChange(ratesViewModel.getCurrenciesData()[position])
            }
        })
    }
}

